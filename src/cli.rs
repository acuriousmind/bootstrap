use std::path::PathBuf;
use structopt::StructOpt;

// The command line options
#[derive(StructOpt, Debug)]
#[structopt(name = "boots")]
pub struct Opt {
  #[structopt(subcommand)]
  pub subcommand: Subcommands,
}

/// The subcommands of the boots CLI
#[derive(Debug, StructOpt)]
pub enum Subcommands {
  #[structopt(name = "list")]
  List(List),
  #[structopt(name = "push")]
  Push(Push),
  #[structopt(name = "setup")]
  Setup(Setup),
  #[structopt(name = "show")]
  Show(Show),
  #[structopt(name = "version")]
  Version,
  #[structopt(name = "dump")]
  Dump(Dump),
  #[structopt(name = "user-data")]
  UserData(UserData),
}

#[derive(Debug, StructOpt)]
pub struct Setup {
  // TODO: this is the bucket name and gets saved
//pub name: String,
}

#[derive(Debug, StructOpt)]
pub struct UserData {
  pub name: String,
}

#[derive(Debug, StructOpt)]
pub struct List {}

#[derive(Debug, StructOpt)]
pub struct Show {
  pub name: String,
}

#[derive(Debug, StructOpt)]
pub struct Dump {
  pub name: String,
  pub version: Option<String>,
}

#[derive(Debug, StructOpt)]
pub struct Push {
  #[structopt(parse(from_os_str))]
  pub file: PathBuf,
}
