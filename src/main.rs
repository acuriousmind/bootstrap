//! A command line tool to manage boot strap files
//! `boots user-data <NAME>` generates a templated user-data for use in AWS
//! `boots track <NAME> <VERSION>` generates a `ref` token pointed to the correct version

mod cfg;
mod cli;
pub mod repository;

use anyhow::Error;
pub use cli::{Opt, Subcommands};
use structopt::StructOpt;

fn main() -> Result<(), Error>{
    let opt = Opt::from_args();
    if let Ok(cfg) = cfg::load() {
        match opt.subcommand {
            Subcommands::Setup(_) => {
                repository::setup(repository::SetupArgs {
                    account: "262265601828".to_string(),
                })
                  .unwrap();
            }
            Subcommands::List(_) => {
                let boots = repository::list(cfg)?;
                for boot in boots.boots {
                    println!("{:?}", boot);
                }
            }
            Subcommands::Push(args) => {
                repository::push(repository::PushArgs { file: args.file })?;
            }
            Subcommands::Show(args) => {
                let version = repository::show(repository::ShowArgs { name: args.name })?;
                println!("{}", version);
            }
            Subcommands::Dump(args) => {
                let contents = repository::dump(repository::DumpArgs {
                    name: args.name,
                    version: args.version,
                })?;
                println!("{}", contents);
            }
            Subcommands::Version => {
                println!("{}", repository::version()?);
            }
            Subcommands::UserData(args) => {
                //TODO: change to a function call (don't mix UI and logic)
                let template = include_str!("user-data.txt");
                let template = template.replace("{0}", &args.name);
                let template = template.replace("{1}", &cfg.aws.bucket);
                println!("{}", template)
            }
        }
        Ok(())
    } else {
        println!("No Config File Found - exiting");
        Ok(())
    }
}
