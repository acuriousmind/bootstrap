use serde::Deserialize;
use toml;
use anyhow::{Error, anyhow};

#[derive(Deserialize)]
pub struct ConfigFile {
  pub meta: Meta,
  pub aws: Aws,
}

#[derive(Deserialize)]
pub struct Meta {
  pub version: u32,
}

#[derive(Deserialize)]
pub struct Aws {
  pub bucket: String,
}

pub fn load() -> Result<ConfigFile, Error> {
  use std::fs::File;
  use std::io::prelude::*;

  let mut f = File::open("Bootstrap.toml").map_err(|err| anyhow!("No file: {:?}", err))?;

  let mut cfg_file = String::new();
  f.read_to_string(&mut cfg_file)
    .expect("something went wrong reading the config file");

  toml::from_str(&cfg_file).map_err(|err| anyhow!("BadDeserialization: {:?}", err))
}
