use crate::cfg::ConfigFile;
use rusoto_core::{Region};
use rusoto_s3::{
  CreateBucketRequest, GetObjectRequest, HeadObjectRequest, ListObjectsV2Request
  , PutBucketPolicyRequest, PutObjectRequest, S3Client, S3
};
use std::path::PathBuf;
use anyhow::{anyhow, Error};

// TODO: move to config
const BUCKET: &'static str = "sh.twosix.com";

pub fn push(args: PushArgs) -> Result<bool, Error> {
  let client = S3Client::new(Region::UsEast1);

  // read repo VERSION
  let version = version().unwrap();

  // check for existance of this version in S3
  if exist(&version) {
    println!("Bump the version file");
    return Err(anyhow!("RepoError::Put"));
  }

  if !args.file.exists() {
    println!("file doesn't exist: {:?}", args.file);
    return Err(anyhow!("RepoError::Put"));
  }

  let name = args.file.file_name().unwrap().to_str().unwrap().to_string();

  let request = PutObjectRequest {
    bucket: BUCKET.clone().to_string(),
    key: format!("bootstraps/{}/{}", name, version),
    body: Some(read(&args)?.into_bytes().into()),
    content_type: Some("text/x-shellscript".to_string()),
    ..Default::default()
  };

  client
    .put_object(request)
    .sync()
    //.map_err(|_| RepoError::Put)
    .expect("update boot");

  // TODO: post version.sha
  let request = PutObjectRequest {
    bucket: BUCKET.clone().to_string(),
    key: format!("bootstraps/{}/{}.sha", name, version),
    body: Some(version.clone().into_bytes().into()),
    content_type: Some("text/plain".to_string()),
    ..Default::default()
  };

  client
    .put_object(request)
    .sync()
    .map_err(|_| anyhow!("RepoError::Put"))
    .expect("update boot.sha");

  // update current
  let request = PutObjectRequest {
    bucket: BUCKET.clone().to_string(),
    key: format!("bootstraps/{}/current", name),
    body: Some(version.into_bytes().into()),
    content_type: Some("text/x-shellscript".to_string()),
    ..Default::default()
  };

  let result = client
    .put_object(request)
    .sync()
    .map_err(|_| anyhow!("RepoError::Put"));

  Ok(result.is_ok())
}

pub fn read(args: &PushArgs) -> Result<String, Error> {
  use std::fs::File;
  use std::io::prelude::*;

  let mut f = File::open(args.file.clone()).expect("BOOT file not found");

  let mut boot_contents = String::new();
  f.read_to_string(&mut boot_contents)
    .expect("something went wrong reading the boot file");

  Ok(boot_contents)
}

pub fn read_sha(args: &PushArgs) -> Result<String, Error> {
  use std::collections::hash_map::DefaultHasher;
  use std::hash::{Hash, Hasher};

  let content = read(args)?;

  let mut s = DefaultHasher::new();
  content.hash(&mut s);
  Ok(s.finish().to_string())
}

pub fn version() -> Result<String, Error> {
  use std::fs::File;
  use std::io::prelude::*;

  let mut f = File::open("VERSION").expect("VERSION file not found");

  let mut version = String::new();
  f.read_to_string(&mut version)
    .expect("something went wrong reading the file");

  Ok(version)
}

pub fn list(cfg: ConfigFile) -> Result<ListBoots, Error> {
  let client = S3Client::new(Region::UsEast1);
  let request = ListObjectsV2Request {
    bucket: cfg.aws.bucket,
    prefix: Some("bootstraps/".to_string()),
    delimiter: Some("/".to_string()),
    ..Default::default()
  };

  let result = client
    .list_objects_v2(request)
    .sync()
    .map_err(|_| anyhow!("RepoError::List"))?;

  let mut boots = Vec::new();

  for obj in result.common_prefixes.unwrap() {
    boots.push(Boot(obj.prefix.unwrap()));
  }

  Ok(ListBoots { boots: boots })
}

fn exist(version: &str) -> bool {
  let client = S3Client::new(Region::UsEast1);
  let name = "server";
  let request = HeadObjectRequest {
    bucket: BUCKET.clone().to_string(),
    key: format!("bootstraps/{}/{}", name, version),
    ..Default::default()
  };

  let result = client
    .head_object(request)
    .sync()
    .map_err(|_| anyhow!("RepoError::Exist"));

  result.is_ok()
}

pub fn show(args: ShowArgs) -> Result<String, Error> {
  use futures::stream::Stream;
  use futures::Future;
  use std::str;

  let client = S3Client::new(Region::UsEast1);

  // get version
  let request = GetObjectRequest {
    bucket: BUCKET.clone().to_string(),
    key: format!("bootstraps/{}/current", args.name),
    ..Default::default()
  };

  let result = client
    .get_object(request)
    .sync()
    .map_err(|_| anyhow!("RepoError::ShowVersion"))?;

  let version = result.body.unwrap().concat2().wait().unwrap();
  let v = str::from_utf8(&version)
    .unwrap()
    .to_string()
    .trim_end()
    .to_string();

  // get actual contents
  let request = GetObjectRequest {
    bucket: BUCKET.clone().to_string(),
    key: format!("bootstraps/{}/{}", args.name, v),
    ..Default::default()
  };

  println!("{:?}", request);
  let result = client
    .get_object(request)
    .sync()
    .map_err(|e| anyhow!("RepoError::ShowContents: {:?}", e))?;

  let contents = result.body.unwrap().concat2().wait().unwrap();

  Ok(
    str::from_utf8(&contents)
      .unwrap()
      .to_string()
      .trim_end()
      .to_string(),
  )
}

pub fn dump(args: DumpArgs) -> Result<String, Error> {
  use futures::stream::Stream;
  use futures::Future;
  use std::str;

  // TODO: clean this up
  let remote_version = show(ShowArgs { name: args.name })?;
  let key = format!("bootstraps/{}", args.version.unwrap_or(remote_version));

  let client = S3Client::new(Region::UsEast1);
  let request = GetObjectRequest {
    bucket: BUCKET.clone().to_string(),
    key: key,
    ..Default::default()
  };

  let result = client.get_object(request).sync().expect("get obj error");

  let stream = result.body.expect("no stream");
  let contents = stream.concat2().wait().expect("no concat2");

  Ok(str::from_utf8(&contents).unwrap().to_string())
}

pub fn dump_sha(args: DumpArgs) -> Result<String, Error> {
  use std::collections::hash_map::DefaultHasher;
  use std::hash::{Hash, Hasher};

  let content = dump(args)?;

  let mut s = DefaultHasher::new();
  content.hash(&mut s);
  Ok(s.finish().to_string())
}

pub fn setup(args: SetupArgs) -> Result<(), Error> {
  let client = S3Client::new(Region::UsEast1);

  // create bucket
  let request = CreateBucketRequest {
    bucket: BUCKET.clone().to_string(),
    acl: Some("private".to_string()),
    ..Default::default()
  };

  client
    .create_bucket(request)
    .sync()
    .map_err(|_| anyhow!("RepoError::SetupBucket"))?;

  // set policy
  let read_policy = include_str!("read_bucket_policy.json");
  let read_policy = read_policy.replace("{BUCKET_NAME}", BUCKET.clone());

  // TODO: an array
  let read_policy = read_policy.replace("{ACCOUNT}", &args.account);

  let request = PutBucketPolicyRequest {
    bucket: BUCKET.clone().to_string(),
    policy: read_policy.to_string(),
    ..Default::default()
  };

  client
    .put_bucket_policy(request)
    .sync()
    .map_err(|e| anyhow!("RepoError::SetupPolicy: {:?}", e))?;

  Ok(())
}

#[derive(Debug)]
pub struct SetupArgs {
  pub account: String,
}

#[derive(Debug)]
pub struct ShowArgs {
  pub name: String,
}

#[derive(Debug)]
pub struct DumpArgs {
  pub name: String,
  pub version: Option<String>,
}

#[derive(Debug)]
pub struct PushArgs {
  pub file: PathBuf,
}

#[derive(Debug)]
pub struct Boot(String);

#[derive(Debug)]
pub struct ListBoots {
  pub boots: Vec<Boot>,
}
