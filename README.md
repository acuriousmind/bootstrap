# Server Bootstrap

> Bootstrap data is hosted on S3, but is super static and has more to do
> with conventional data structures than "real" application logic.

Terms:

- REF
- NAME
- VERSION

## Config File

```toml
[meta]
version = 22
[aws]
bucket = "bootstrap.twosix.io"
```

## src/bootstrap

Minimal bash script needed to bootstrap a beta server

**It is an explicit goal that this process be as minimal as possible. This
script ideally never changes, except for minor improvements. This should be a
solved problem. **

## Setup

```
boots setup <BUCKET NAME> --account <ACCOUNT> --account <ACCOUNT>
```

## Your project should have a place to store your bootstrap scripts

Bootstrap doesn't care what OS / architecture you have. This is all for
downstream systems to be concerned about.

```
~/
  src/
    xenial
    trusty
```

## Publish

```
boots push <PATH> --version VERSION
boots push src/xenial #auto increments version
boots push src/xenial --version 1 #pushes to version 1
```

Goes to `~/bootstraps/xenial/1`
and `~/bootstraps/xenial/current` is set to `1`

## List Options

```
> boots list

Arch  OS     Version  SHA
x86   xenial       4  deadbeef
arm   xenial       2  deadbeef
x86   trusty      22  deadbeef
```

## Show Script

```
boots show <NAME> <VERSION>
boots show xenial   # shows current
boots show xenial 4 # shows a specific version
```

# Integrate your EC2 with Bootstrap

In the user data, all you need is the following

```sh
#!/bin/bash -v

# sets up a full run of logging
exec > >(tee /tmp/user-data.log | logger -t user-data -s 2>/dev/console) 2>&1

export BEACON="s._.twosix.io"
echo "export BEACON=s._.twosix.io" >> /etc/environment
export CHANNEL="stable"
echo "export CHANNEL=stable" >> /etc/environment

export BOOTSTRAP_BUCKET="bootstraps.${CHANNEL}._.twosix.io"
echo "export BOOTSTRAP_BUCKET=bootstraps.${CHANNEL}._.twosix.io" >> /etc/environment

export BOOTSTRAP_REF="<BOOT NAME>"
echo "BOOTSTRAP_REF=<BOOT NAME>" >> /etc/environment

curl "https://${BOOTSTRAP_BUCKET}/bootstrap" -sSf | sh
```

## S3 Object Storage Layout

- bootstraps.stable._.twosix.io
- bootstraps.beta._.twosix.io
- bootstraps.alpha._._twosix.io

```
~/
  bootstrap     # root bootstrap script
  bootstraps/   # bootstrap scripts
    <NAME>/
      CURRENT   # contains the most recent sha value
      <VERSION> # individual bootsrap script
```

example

```text
~/
  bootstrap       # root bootstrap script
  bootstraps/     # bootstrap scripts
    xenial-arm/
      current    # contains the most recent sha value
      deadbeef   # individual bootstrap script
    xenial-x86/
      current    # contains the most recent sha value
      deadbeef   # individual bootstrap script
```